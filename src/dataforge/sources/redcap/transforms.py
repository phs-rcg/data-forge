"""Transformations applied to data exported from a REDCap project

These transformations assume a Pandas DataFrame and a corresponding schema, as
opposed to an actual resource object. The primary reason for this is that it
permits easy use of Pandas for data manipulation; moreover, these are intended
solely for the purpose of transforming data coming directly from REDCap.
"""

import numpy as np
import pandas as pd
from ...tools import date_offset

def transform_data(form, tabular_data, shift_dates=False, dag='data_access_groups',
                   status=True, rename_checkbox_fields=False, **kwargs):
    """Apply transforms to tabular data resource to improve usability.

    Argument tabular_data is a named tuple with fields df and schema; thus, it
    is immutable and its components need to be modified in place.
    """

    _strip_whitespace(tabular_data.df)

    mapper = {}
    if rename_checkbox_fields:
        mapper = _rename_checkboxes(form, tabular_data)

    drop = []
    if not status:
        drop.append(f'{form.form_name}_complete')

    if dag:
        mapper['redcap_data_access_group'] = dag
    else:
        drop.append('redcap_data_access_group')

    _convert_dates(form, tabular_data)
    if shift_dates:
        if 'shift' in kwargs:
            offset = kwargs.get('shift')
        else:
            key = tabular_data.df.index.get_level_values(level=form.project.record_id)
            offset = date_offset(key, min_days=kwargs.get('min_days', -354),
                                 max_days=kwargs.get('max_days', -1),
                                 seed=kwargs.get('shift_seed'),
                                 name=kwargs.get('shift_name', '_offset'),
                                 repo_path=kwargs.get('repo_path', 'de-identification'),
                                 **kwargs)
        tabular_data.df.dataforge.shift_dates(offset, inplace=True)

    _rename_fields(tabular_data, mapper)
    _drop_fields(tabular_data, drop)

def _strip_whitespace(df):
    """Strip leading/trailing whitespace and embedded newlines in data frame."""

    # Trim leading and trailing whitespace
    df.replace({r'^\s+':'', r'\s+$':''}, regex=True, inplace=True)

    # Remove embedded newlines
    df.replace(r'\r?\n', '  ', regex=True, inplace=True)

def _rename_checkboxes(form, tabular_data):
    """Map names of checkbox items to simpler alternatives."""

    mapper = {}
    for item in form.items:

        if item.field_type=='checkbox':

            varname = item.varname
            if len([c for c in tabular_data.df.columns
                    if c.startswith(varname + '___')])>1:
                mapper[item.name] = varname + item.name[len(varname)+3:]
            else:
                mapper[item.name] = varname

    return mapper

def _convert_dates(form, tabular_data):
    """Convert REDCap data and datetime fields into datetime64[ns]"""

    df = tabular_data.df
    for item in form.items:

        if item.data_type in ['date','datetime','partialDatetime']:
            df[item.varname] = pd.to_datetime(df[item.varname])

def _rename_fields(tabular_data, mapper):
    """Rename one or more field(s) in tabular data resource."""

    df, schema = tabular_data

    df.index.names = list(map(lambda name: mapper.get(name, name), df.index.names))
    df.rename(columns=mapper, inplace=True)

    # TODO Rename fields in uniqueKeys and foreignKeys too, if necessary
    for field in schema['fields']:
        name = field['name']
        if name in mapper:
            field['name'] = mapper[name]
            if name in schema['primaryKey']:
                schema['primaryKey'] = [mapper.get(f, f) for f in schema['primaryKey']]

def _drop_fields(tabular_data, drop):
    """Drop one or more field(s) in tabular data resource."""

    # TODO Handle case where deleted field(s) are in uniqueKeys or foreignKeys
    df, schema = tabular_data
    df.drop(columns=drop, inplace=True)
    schema['fields'] = [field for field in tabular_data.schema['fields']
                        if field['name'] not in drop]