"""Utilities for working with Frictionless data"""

from frictionless import Schema
from pathlib import Path
from collections.abc import Mapping

def write_stata_script(resource, path=None, value_labels=None,
                       vl_from_enum=True, mv_ignore=None, version=None,
                       **kwargs):
    """Write Stata script to read and format tabular data according to schema.

    Parameters
    ----------
    path : str or path object, optional
        File into which script will be written.
    value_labels : str, path object or dict, optional
        Contains value label definitions and assignment to fields.
    vl_from_enum : bool, default True
        Automatically generate value labels from fields with enum constraint.
    mv_ignore : list of str, optional
        Values in missingValues that should be ignored when creating
        value labels.
    version : str, optional
        Version identifier to be added to dataset label
    **kwargs
        Additional keywords passed through to _format_cmds().

    Notes
    -----
    Resource must contain path to CSV file.
    """

    # TODO Handle enumLabels and enumOrdered

    if not resource.path:
        raise Exception("Resource does not contain path to CSV file")

    if mv_ignore is None:
        mv_ignore = [""]

    value_labels, fld_map = _value_labels(resource, value_labels)
    format_cmds = _format_cmds(resource, fld_map, vl_from_enum, mv_ignore,
                               **kwargs)

    if value_labels or format_cmds:
        header = (f'// Read tabular resource "{resource.name}"\n\n'
                  "version 17\n"
                  "drop _all\n"
                  "label drop _all\n"
                  "program drop _all\n")
        with open(Path(__file__).parent / "format_from_schema.ado") as file:
            ado = file.read()
    else:
        header = (f'// Read tabular resource "{resource.name}"\n\n'
                  "version 17\n"
                  "drop _all\n"
                  "label drop _all")
        ado = ""

    script = "\n".join([header, ado, _read_csv_file(resource),
                        *value_labels, *format_cmds,
                        _closing(resource, value_labels, version)])

    if not path:
        path = Path('scripts') / (resource.name + ".do")
    elif Path(path).suffix != ".do":
        path = Path(path).append_suffix('.do')
    else:
        path = Path(path)

    path.parent.mkdir(parents=True, exist_ok=True)
    with open(path, "w") as file:
        file.write(script)

def _value_labels(resource, value_labels):
    """Return value labels and field map."""

    # TODO Handle enum fields when value labels not provided

    if value_labels:
        try:
            value_labels = _read_value_labels(Path(value_labels))
        except TypeError:
            if not isinstance(value_labels, Mapping):
                raise TypeError("expected str, os.PathLike object, or mapping")

        try:
            for k, v in value_labels['field_to_value_label'].items():
                if v not in value_labels.get('value_labels', {}):
                    raise Exception(f"Field '{k}' mapped to undefined label")
        except KeyError:
            raise Exception("value_labels does not contain 'field_to_value_label'")

        fld_map = value_labels['field_to_value_label']
        if resource.schema:
            # If schema provided, only those labels mapped to a field in
            # the schema are retained
            fld_map = {k:v for (k,v) in fld_map.items()
                       if k in [i.name for i in resource.schema.fields]}

            # Verify that all enumerated values are labeled
            for field in [f for f in resource.schema.fields if f.name in fld_map]:
                if field.constraints and ('enum' in field.constraints):
                    vlab = value_labels['value_labels'][fld_map[field.name]]
                    labs = set(vlab.values())
                    if not set(field.constraints['enum']).issubset(labs):
                        raise Exception("One or more enumerated values of "
                                        f"field '{field.name}' not labeled")
        return ([_value_label(k,v) for (k,v)
                     in value_labels['value_labels'].items()
                     if k in list(fld_map.values())], fld_map)
    else:
        return ([], {})

def _read_value_labels(path):
    """Read value labels and field map from file."""

    with open(path) as file:
        if path.suffix==".json":
            import json
            value_labels = json.load(file)
        elif path.suffix==".yaml":
            import yaml
            value_labels = yaml.safe_load(file)
        else:
            raise Exception(f"Expected .json or .yaml file")

    return value_labels

def _value_label(name, label):
    """Return value label."""

    code = f"lab def {name} "
    indent = " " * len(code)
    first = True
    for v in label:
        if first:
            first = False
        else:
            prefix = indent[:-1] if str(v).startswith(".") else indent
            code = code + " ///\n" + prefix
        code = code + f"""{v} `"{label[v]}"'"""

    return code + "\n"

def _format_cmds(resource, fld_map, vl_from_enum, mv_ignore, fld_list=None,
                 **kwargs):
    """Return Stata commands to format fields.

    Parameters
    ----------
    fld_list : list, optional
        Limit format commands to fields in list.
    **kwargs
        Additional keywords passed through to _format_cmds().
    """

    cmds = []
    if resource.schema:
        for field in resource.schema.fields:
            if fld_list is not None and field.name not in fld_list:
                continue
            cmds.append(_format_cmd(field=field,
                                    mvals=resource.schema.missing_values,
                                    label=fld_map.get(field.name),
                                    vl_from_enum=vl_from_enum,
                                    mv_ignore=mv_ignore, **kwargs))
    elif fld_map:
        for field in fld_map:
            if fld_list is not None and field not in fld_list:
                continue
            cmds.append(_format_cmd(name=field, label=fld_map[field], **kwargs))

    return cmds

def _format_cmd(field=None, name=None, mvals=None, label=None,
                vl_from_enum=False, mv_ignore=None, **kwargs):
    """Return Stata command to format a field."""

    if not (field or name):
        raise Exception("One of field or name required")
    name = field.name if field else name

    opts = []
    type_format_label = []

    if field:
        if field.type:
            type_format_label.append(f"type({field.type})")
        if field.format:
            type_format_label.append(f"""format(`"{field.format}"')""")
        if vl_from_enum and not label:
            type_format_label.append("vl_from_enum")
            if mv_ignore:
                type_format_label.append(f"mv_ignore({_fmt_list(mv_ignore)})")
        if field.constraints and ('enum' in field.constraints):
            enum = _fmt_list(field.constraints['enum'])
            opts.append(f"enum({enum})")
        if mvals:
            opts.append(f"missing_vals({_fmt_list(mvals)})")
        if field.title:
            opts.append(f"""title(`"{field.title}"')""")
        if field.description:
            opts.append(f"""description(`"{field.description}"')""")

    if label:
        type_format_label.append(f"label({label})")

    if kwargs.get("float", None):
        type_format_label.append("float")

    opts.insert(0, " ".join(type_format_label))

    # Note: opts will never be empty, since any field will by definition
    # have to occur in a schema (which guarantees a default type and format)
    # or be mapped to a value label.
    return f"format_from_schema {name}, " + " ///\n    ".join(opts) + "\n"

def _fmt_list(lst):
    """Format list so it can be passed as value to Stata option"""

    items = []
    for item in lst:
        items.append(f"""`"{item}"'""")

    return " ".join(items)

def _read_csv_file(resource):
    """Return code to read CSV file."""

    # TODO Handle dialect and encoding

    code = f"""qui import delim using `"{resource.path}"', """
    opts = ["case(preserve)"]

    if resource.schema:
        opts.append("stringcols(_all)")

    return code + " ".join(opts) + "\n"

def _closing(resource, value_labels, version):
    """Return closing steps."""

    if resource.schema or value_labels:
        code = "program drop _all\ncompress\n"
    else:
        code = "compress\n"

    if resource.schema and resource.schema.primary_key:
        code = code + f"""isid {' '.join(resource.schema.primary_key)}, sort\n"""
    code = code + "datasig set\n"

    if resource.title or version:
        label = resource.title if resource.title else ''
        label = label + f' (v{version})' if (label and version) else (
                f'v{version}' if version else None)
        if label:
            code = code + f"""\nlab dat `"{label}"'\n"""
    if resource.description:
        code = code + f"""\nnote: {resource.description}\n"""

    return code

def _combine_lists(list1, list2):
    """Combine two lists, retaining as much of the original order as possible."""

    merged = []
    startpos = 0

    for item in list1:
        if item not in merged:
            if item in list2:
                pos = list2.index(item)
                merged += list2[startpos:pos]
                startpos = pos + 1
            merged.append(item)

    merged += list2[startpos:]
    return merged

def combine_schemas(schemas, ignore_dups=None):
    """Combine Frictionless Table Schemas.

    Assumes that (1) each field appears in only one schema; (2) missingValues
    may be combined across schemas; (3) primaryKey, if specified, is consistent
    across schemas; and (4) foreignKeys, if specified, may be combined across
    schemas. Primary key fields are moved to the front.
    """

    if isinstance(ignore_dups, str):
        ignore_dups = [ignore_dups]
    elif not ignore_dups:
        ignore_dups = []

    if len(schemas) == 0:
        return Schema()

    new_schema = Schema()
    if schemas[0].fields:
        new_schema.fields = schemas[0].fields
    if schemas[0].missing_values:
        new_schema.missing_values = schemas[0].missing_values
    if schemas[0].primary_key:
        new_schema.primary_key = schemas[0].primary_key
    if schemas[0].foreign_keys:
        new_schema.foreign_keys = schemas[0].foreign_keys

    for schema in schemas[1:]:

        for field_name in schema.field_names:
            if new_schema.has_field(field_name):
                if field_name not in new_schema.primary_key and field_name not in ignore_dups:
                    raise Exception(f'Field "{field_name}" found in two schema')
            else:
                new_schema.add_field(schema.get_field(field_name))

        if schema.missing_values != ['']:
            if new_schema.missing_values != ['']:
                new_schema.missing_values = _combine_lists(new_schema.missing_values,
                                                           schema.missing_values)
            else:
                new_schema.missing_values = schema.missing_values

        if schema.primary_key:
            if schema.primary_key != new_schema.primary_key:
                minlen = min(len(schema.primary_key), len(new_schema.primary_key))
                if schema.primary_key[:minlen] != new_schema.primary_key[:minlen]:
                    raise Exception(f'Primary keys {new_schema.primary_key} ' +
                                    f'and {schema.primary_key} are inconsistent')
                if len(schema.primary_key) > len(new_schema.primary_key):
                    new_schema.primary_key = schema.primary_key

        if schema.foreign_keys:
            for key in schema.foreign_keys:
                if key not in new_schema.foreign_keys:
                    new_schema.foreign_keys.append(key)

    key_fields = []
    for field in new_schema.primary_key:
        key_fields.append(new_schema.get_field(field))
        new_schema.remove_field(field)
    new_schema.fields = key_fields + new_schema.fields

    return new_schema
