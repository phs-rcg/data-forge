"""A set of general tools for use in working with data"""

import os
from pathlib import Path
import contextlib
from urllib.parse import urlparse
from git import Repo
from git.exc import NoSuchPathError
import numpy as np
import pandas as pd
from dataforge import config

@contextlib.contextmanager
def versioned_file_resource(filename, remote_url=None, repo_path=None, mode='r',
                            empty_ok=False, verbose=False):
    """Open versioned file and return corresponding file object.

    We handle the following important cases (as well as several others):

    1. Only remote repository specified; local clone is temporary
    2. Only local clone specified, possibly handled as a git submodule to
       separate shared code from secret information for deidentification, with
       or without remote
    3. User specifies nothing, local repository created automatically in ./tmp
    """
    _echo = print if verbose else lambda *a, **k: None

    if not repo_path:

        if remote_url:
            repo_name = os.path.basename(urlparse(remote_url).path)
            repo_path = Path(os.path.join('tmp/git', repo_name)).with_suffix('')

        else:
            repo_path = Path(os.path.join('tmp/git/dataforge', repo_name))

    try:
        repo = Repo(repo_path)
        assert not repo.is_dirty(untracked_files=True)
        if remote_url:
            # must correspond to first remote defined in .git/config
            assert remote_url == repo.remotes[0].url

        if repo.remotes:
            if repo.references:
                repo.remotes[0].pull()
                _echo(f'Pulled latest changes from {repo.remotes[0].url} into {repo_path}')
            else:
                # Pulling into an empty repo yields git error
                _echo(f'Pull skipped because local repository {repo_path} is empty')

    except NoSuchPathError:

        if remote_url:
            repo = Repo.clone_from(remote_url, repo_path)
            _echo(f'Cloned {remote_url} into {repo_path}')

        else:
            repo = Repo.init(repo_path)
            _echo(f'Initialized new repository in {repo_path}')

    file_path = os.path.join(repo_path, filename)
    # Use newline='' to disable universal newline support
    file_obj = open(file_path, mode, newline='')

    try:
        yield file_obj

    except Exception as e:
        if repo:
            file_obj.close()
            repo.git.reset('--hard')
        raise e

    finally:
        file_obj.close()
        file_size = os.path.getsize(file_path) if os.path.exists(file_path) else None

        # Don't store empty file
        if not file_size and not empty_ok:
            with contextlib.suppress(FileNotFoundError):
                os.remove(file_path)

        if repo.is_dirty(untracked_files=True):
            repo.git.add('--all')
            repo.index.commit('Commit by versioned_file_resource()')
            _echo(f'Changes to {file_path} committed to local repository {repo_path}')

            if repo.remotes:
                try:
                    repo.remotes[0].push()
                    _echo(f'Changes pushed to remote {repo.remotes[0].url}')
                except Exception as e:
                    repo.git.reset('--hard')
                    repo.head.reset('HEAD~1', index=True, working_tree=True)
                    raise e

@pd.api.extensions.register_dataframe_accessor('dataforge')
class DataForgeAccessor:
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def shift_dates(self, shift, date_cols=None, on=None, inplace=False):
        """Shift dates in data frame around an index date or by an offset

        If shift is a date, then resulting columns will be integers
        representing the number of days from the index; if shift is an offset,
        then resulting columns will be dates. By default all date columns in
        data frame are shifted; this can be limited by use of date_cols.

        Parameters
        ----------
        shift : pandas.Series
            Series of type datetime64 or int indicating offset in days
        date_cols : str or list of str, optional
            Name(s) of date column(s) to be shifted
        on : str or list of str, optional
            Column or index level name(s) in data frame to join on the index in
            shift, otherwise joins index-on-index. If multiple values given,
            shift must have a MultiIndex.
        inplace : bool
            If True, performs operation inplace and returns None.

        Returns
        -------
        pandas.DataFrame
            Data frame with shifted dates
        """

        df = self._obj.join(shift, on=on, how='left', validate='many_to_one')
        shift_col = df.iloc[:,-1]

        if date_cols:
            date_cols = [date_cols] if isinstance(date_cols, str) else date_cols
        else:
            date_cols = [c for c, is_type in
                         (self._obj.dtypes=='datetime64[ns]').items()
                         if is_type]

        if shift_col.dtype=='datetime64[ns]':
            for col in date_cols:
                if inplace:
                    self._obj[col] = (df[col] - shift_col).dt.days
                else:
                    df[col] = (df[col] - shift_col).dt.days
        else:
            shift_col = pd.to_timedelta(shift_col, unit='days')
            for col in date_cols:
                if inplace:
                    self._obj[col] = df[col] + shift_col
                else:
                    df[col] = df[col] + shift_col

        if not inplace:
            return df.iloc[:,:-1]

def date_offset(key, offset_file='date_offset.csv', offset_url=None, min_days=-182,
                max_days=183, seed=None, name='_offset', repo_path=None,
                verbose=False):
    """Generate and store random offset (in days) for use in shifting dates

    Parameters
    ----------
    key : pandas.Series or pandas.DataFrame
        One or more columns uniquely identifying entities for which offsets
        are to be generated; duplicates and rows with missing values will be
        removed
    offset_file : str
        Name of file in which offsets are stored for subsequent use
    offset_url : str, optional
        URL of bare git repository containing offset generation history
    min_days : int, default -182
        Minimum possible shift (in days)
    max_days : int, default 183
        Maximum possible shift (in days)
    seed : int, array_like[ints], SeedSequence, BitGenerator, Generator, optional
        A seed to initialize the BitGenerator
    name : str, default '_offset'
        Name of column in offset_file containing offsets
    repo_path : str
        Path to local repository containing previously generated offsets

    Returns
    -------
    pandas.Series
        Series containing offsets with key as index
    """

    if not offset_url:
        offset_url = config['offset_url'].get(str)

    key = pd.DataFrame(key).dropna().drop_duplicates(ignore_index=True).astype('object')
    rng = np.random.default_rng(seed=seed)

    with versioned_file_resource(offset_file, remote_url=offset_url,
                                 repo_path=repo_path, mode='a+', verbose=True) as f:

        f.seek(0)
        try:
            offsets = pd.read_csv(f, dtype='object')
            add_header = False
        except pd.errors.EmptyDataError:
            offsets = pd.DataFrame(columns = key.columns.values.tolist() + [name],
                                   dtype='object')
            add_header = True

        offsets = (key
                   .merge(offsets, how='left', on=key.columns.values.tolist(),
                          validate='one_to_one', indicator=True)
                   .sort_values(by=key.columns.values.tolist(), ignore_index=True)
                   .astype({name:'Int64'})
                   .pipe(lambda df: df.fillna(
                       pd.DataFrame(rng.integers(low=min_days, high=max_days+1,
                                                 size=len(df)), columns=[name])))
                  )

        if (offsets._merge=='left_only').any():
            (offsets.loc[offsets._merge=='left_only', offsets.columns!='_merge']
             .to_csv(f, index=False, header=add_header))

        return offsets.iloc[:,:-1].set_index(key.columns.values.tolist(),
                                             verify_integrity=True).squeeze(1)
