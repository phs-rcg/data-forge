.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/data-forge.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/data-forge
    .. image:: https://readthedocs.org/projects/data-forge/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://data-forge.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/data-forge/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/data-forge
    .. image:: https://img.shields.io/pypi/v/data-forge.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/data-forge/
    .. image:: https://img.shields.io/conda/vn/conda-forge/data-forge.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/data-forge
    .. image:: https://pepy.tech/badge/data-forge/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/data-forge
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/data-forge

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

=========
Dataforge
=========

.. note:: Tested with Python 3.11.

Dataforge is a Python package containing tools for creating and packaging data
products.

.. contents::
   :depth: 2


Installation
============

.. note:: Installation in a virtual environment of the type created by the
          `venv module <https://docs.python.org/3/library/venv.html>`_ or
          `virtualenv <https://virtualenv.pypa.io/en/latest/>`_ is highly
          recommended.

To install dataforge::

    pip install dataforge

Optional `Frictionless <https://framework.frictionlessdata.io>`_ components
can be enabled by installing with the ``frictionless`` option::

    pip install dataforge[frictionless]

Optional `REDCap`_ support requires
`xmarshal <https://github.com/pschumm/xmarshal>`_, which must be compiled using
`Cython <https://cython.readthedocs.io/>`_. Thus, prior to installation, you
will need a C compiler:

Linux
    ``sudo apt-get install build-essential``

Mac OS X
    Install Apple’s XCode from the App Store

Windows
    Install `Microsoft's C++ Build Tools <https://visualstudio.microsoft.com/visual-cpp-build-tools/>`_

Once you have a C compiler, you may install dataforge with REDCap support
using::

    pip install dataforge[redcap]


Configuration
=============

For ease of use, many arguments and options for executable commands as well as
many function arguments may be specified in a file called ``config.yaml``
located at the root of your project. For example, you could specify arguments
and options for the ``redcap_export`` command by creating a ``config.yaml``
file containing something like this::

    sources.redcap.api:
        url: https://redcap.uchicago.edu/api/
        project_name: MyREDCapProject
        outdir: tmp/redcap


REDCap
======

Management of API Tokens
------------------------

To access a REDCap project via the API, you will need to obtain a REDCap API
token. To avoid having to enter the token each time you export data, it is
convenient to add the token to your system keyring. You may do this with the
following command::

    keyring set <redcap_hostname> [<project_name>]

where ``<redcap_hostname>`` is the hostname of your REDCap server (e.g.,
``redcap.uchicago.edu``) and ``<project_name>`` is an abbreviated (no spaces)
name of the REDCap project (e.g., ``MyREDCapProject``). Although specifying a
project name is not required, doing so will allow you to store API tokens for
multiple projects. When you are asked to enter a password, enter the API token
you obtained for your project.

.. warning:: Do not store your API token in a plaintext or otherwise
             unencrypted file, and do not write it down on a piece of paper.
             If another person were to obtain your API token, he or she would
             have the same access to your REDCap project as you do. If there
             is ever a possibility that your token has been compromised, you
             should immediately regenerate your token.


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.6. For details and usage
information on PyScaffold see https://pyscaffold.org/.
